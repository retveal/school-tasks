#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct flag {
    bool b;
    bool e;
    bool n;
    bool s;
    bool t;
    bool v;
} option;

void parser(int argc, char *argv[], option *flag);
void scan_files(int argc, char *argv[], option *flag);
void flag_handling(FILE *file, option *flag, char current_char);
void s_flag(char current_char, char previous_char, int *lines_count, int *print_char);
void e_flag(char current_char);
void b_flag(char current_char, char previous_char, int *line_number);
void n_flag(char previous_char, int *line_number);
void t_flag(char current_char, int *print_char);
void v_flag(char current_char);

int main(int argc, char *argv[]) {
    // all flags are set to false
    option flag = {false};
    flag.v = false;
    
    parser(argc, argv, &flag);
    scan_files(argc, argv, &flag);
    
    return 0;
}

void parser(int argc, char *argv[], option *flag) {
    
    static struct option long_options[] = {
        { "number-nonblank", no_argument, NULL, 'b'},
        { "number", no_argument, NULL, 'n'},
        { "squeeze-blank", no_argument, NULL, 's'},
        { NULL, 0, NULL, 0}
    };
    
    int character;
    
    while ((character = getopt_long(argc, argv, "+benstvTE", long_options, NULL)) != -1) {
           switch (character) {
               case 'b':
                   flag -> b = true;
                   break;
               case 'e':
                   flag -> e = true;
                   flag -> v = true;
                   break;
               case 'E':
                   flag -> e = true;
                   break;
               case 'n':
                   flag -> n = true;
                   break;
               case 's':
                   flag -> s = true;
                   break;
               case 't':
                   flag -> t = true;
                   flag -> v = true;
                   break;
               case 'T':
                   flag -> t = true;
                   break;
               case 'v':
                   flag -> t = true;
                   break;
               default:
                   fprintf(stderr, "sorry");
                   exit(1);
           }
       }
}

void scan_files(int argc, char *argv[], option *flag) {
    FILE *file;
    char current_char;

    for (int i = optind; i < argc; i++) {
        file = fopen(argv[i], "r");
        
        if (file != NULL) {
            flag_handling(file, flag, current_char);
        } else {
            fprintf(stderr, "No such file or directory");
            exit(1);
        }
        
        fclose(file);
    }
}

void flag_handling(FILE *file, option *flag, char current_char) {
    int print_char = 0, previous_char = '\n', line_number = 1, scip_spaces = 0;;
    
    while ((current_char = fgetc(file)) != EOF) {
        if (flag -> s) {
            s_flag(current_char, previous_char, &scip_spaces, &print_char);
        }
        
        if (flag -> e) {
            e_flag(current_char);
        }
        
        if (flag -> b) {
            b_flag(current_char, previous_char, &line_number);
        }
        
        if (flag -> n) {
            n_flag(previous_char, &line_number);
        }
        
        if (flag -> t) {
            t_flag(current_char, &print_char);
        }
        
        if (flag -> v) {
            printf("v]]]]]]");
            v_flag(current_char);
        }
        
        if (!print_char) {
            putchar(current_char);
        }
        
        print_char = 0;
        previous_char = current_char;
    }
}

void s_flag(char current_char, char previous_char, int *lines_count, int *print_char) {
    if (previous_char == '\n' && current_char == '\n') {
        (*lines_count)++;
        if (*lines_count > 1) {
            *print_char = 1;
            *lines_count = 0;
        }
    }
}

void e_flag(char current_char) {
    if (current_char == '\n') {
        putchar('$');
    }
}

void b_flag(char current_char, char previous_char, int *line_number) {
    if (previous_char == '\n' && previous_char != current_char) {
        printf("%6d\t", (*line_number)++);
    }
}

void n_flag(char previous_char, int *line_number) {
    // if the previous character is equal to '\n'
    if (previous_char == '\n') {
        // print lines count
        printf("%6d\t", (*line_number)++);
    }
}

void t_flag(char current_char, int *print_char) {
    if (current_char == '\t') {
        printf("^I");
        *print_char = 1;
    }
}

void v_flag(char current_char) {
    if ((current_char >= 0 && current_char <= 8)
        // or between eleven and thirty-one
        // the thirty-second is space
        || (current_char >= 11  && current_char <= 31)
        // if current character is equal to one hundred and twenty-seven
        // the one hundred and twenty-seven - delete
        || (current_char == 127)) {
        // print ^ and the symbol
        putchar('^');
        if (current_char == 127) {
            // print ?
            current_char -= 64;
        } else {
            current_char += 64;
        }
    }
}

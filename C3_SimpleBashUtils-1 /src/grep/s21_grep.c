#include <stdio.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdlib.h>
#include <regex.h>

#define MAX_SIZE 2048

typedef struct flag {
    bool e;
    bool i;
    bool v;
    bool c;
    bool l;
    bool n;
    bool h;
    bool s;
    bool o;
} option;

//int parser(int argc, char *argv[], option *flag);
void argforgrep(int argc, char *argv[], char *pattern);

int main(int argc, char *argv[]) {
    char pattern[4096] = {0};
    option flag = {false};
//    parser(argc, argv, &flag);
    argforgrep(argc, argv, pattern);
    return 0;
}


void argforgrep (int argc, char *argv[], char *pattern) {
    FILE *file;
    
    char string[MAX_SIZE];
    
    regex_t regex = {0};
    
    if (argc < 3) {
        printf("error");
    } else {
        if (regcomp(&regex, pattern, REG_EXTENDED) == 0) {
            for (int i = optind; i < argc; i++) {
               file = fopen(argv[i], "r");
               
                if (file == NULL) {
                   fprintf(stderr, "No such file or directory");
                   exit(1);
               }
                
                while (fgets(string, MAX_SIZE, file) != NULL) {
                    if (regexec(&regex, string, 0, NULL, 0) == 0) {
                        printf("%s", string);
                    }
                }
                
                fclose(file);
            regfree(&regex);
           }
        }
    }
}

//int parser(int argc, char *argv[], option *flag) {
//
//    static struct option long_options[] = {
//        { NULL, 0, NULL, 0}
//    };
//
//    int character;
//
//    while ((character = getopt_long(argc, argv, "e:ivclnhsof:", long_options, NULL)) != -1) {
//        switch (character) {
//            case 'e':
//                flag -> e = true;
//                break;
//            case 'i':
//                flag -> i = true;
//                break;
//            case 'v':
//                flag -> v = true;
//                break;
//            case 'c':
//                flag -> c = true;
//                break;
//            case 'l':
//                flag -> l = true;
//                break;
//            case 'n':
//                flag -> n = true;
//                break;
//            case 'h':
//                flag -> h = true;
//                break;
//            case 's':
//                flag -> s = true;
//                break;
//            case 'o':
//                flag -> o = true;
//                break;
//            default:
//                fprintf(stderr, "sorry");
//                exit(1);
//        }
//    }
//}
